<?php

namespace Core\Http;

/**
 * Class Request an http request
 *
 * @package Http
 */
class Request {

    /**
     *  Get COOKIE Super Global
     *
     * @var
     */
    public $cookie;

    /**
     *  Get REQUEST Super Global
     *
     * @var
     */
    public $request;

    /**
     *  Get FILES Super Global
     *
     * @var
     */
    public $files;

    /**
     * Request data type
     *
     * @var string|mixed
     */
    protected string $type;

    /**
     * Request constructor.
     */
    public function __construct() {
        $this->request = $this->clean($_REQUEST);
        $this->cookie = $this->clean($_COOKIE);
        $this->files = $this->clean($_FILES);

        $this->type = $this->clean($_SERVER['REQUEST_METHOD']);
    }

    /**
     *  Get $_GET parameter
     *
     * @param String $key
     * @return string
     */
    public function get(String $key = '') : string {
        if ($key != '')
            return isset($_GET[$key]) ? $this->clean($_GET[$key]) : '';

        return  $this->clean($_GET);
    }

    /**
     *  Get $_POST parameter
     *
     * @param String $key
     * @return string
     */
    public function post(String $key = '') : string {
        if ($key != '')
            return isset($_POST[$key]) ? $this->clean($_POST[$key]) : '';

        return  $this->clean($_POST);
    }

    /**
     *  Get POST parameter
     *
     * @param String $key
     * @return string
     */
    public function input(String $key = '') : string {
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata, true);

        if ($key != '') {
            return isset($request[$key]) ? $this->clean($request[$key]) :'';
        }

        return ($request);
    }

    /**
     *
     * @return string
     */
    public function getType() : string {
        return $this->type;
    }

    /**
     *  Get value for server super global var
     *
     * @param String $key
     * @return string
     */
    public function server(String $key = '') : string {
        return isset($_SERVER[strtoupper($key)]) ? $this->clean($_SERVER[strtoupper($key)]) : $this->clean($_SERVER);
    }

    /**
     *  Get Method
     *
     * @return string
     */
    public function getMethod() : string {
        return strtoupper($this->server('REQUEST_METHOD'));
    }

    /**
     *  Returns the client IP addresses.
     *
     * @return string
     */
    public function getClientIp() : string {
        return $this->server('REMOTE_ADDR');
    }

    /**
     *  Script Name
     *
     * @return string
     */
    public function getUrl() : string {
        return $this->server('REQUEST_URI');
    }

    /**
     * Clean Data
     *
     * @param $data
     * @return
     */
    private function clean($data) {
        if (is_array($data)) {
            foreach ($data as $key => $value) {

                // Delete key
                unset($data[$key]);

                // Set new clean key
                $data[$this->clean($key)] = $this->clean($value);
            }
        } else {
            $data = htmlspecialchars($data, ENT_COMPAT, 'UTF-8');
        }

        return $data;
    }
}
