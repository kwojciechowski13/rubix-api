<?php

namespace Core\Core;

use Core\Http\Response;
use Core\Http\Request;

/**
 * Base controller
 *
 */
abstract class Controller
{

    /**
     * Parameters from the matched route
     * @var array
     */
    protected array $route_params;

    /**
     * @var object|Response
     */
    protected object $response;

    /**
     * @var object|Request
     */
    protected object $request;

    /**
     * @var object
     */
    protected object $config;

    /**
     * Class constructor
     *
     * @param array $route_params  Parameters from the route
     *
     * @return void
     */
    public function __construct(array $route_params = [])  {
        $this->route_params = $route_params;

        $this->request = new Request();
        $this->response = new Response();

        $this->config = \App\Config\Config::getInstance();
    }

    /**
     * Magic method called when a non-existent or inaccessible method is
     * called on an object of this class. Used to execute before and after
     * filter methods on action methods. Action methods need to be named
     * with an "Action" suffix, e.g. indexAction, showAction etc.
     *
     * @param string $name  Method name
     * @param array $args Arguments passed to the method
     *
     * @return void
     */
    public function __call($name, $args) : void
    {
        $method = $name . 'Action';

        if (method_exists($this, $method)) {
            if ($this->before() !== false) {
                call_user_func_array([$this, $method], $args);
                $this->after();
            }
        } else {
            throw new \Exception("Method $method not found in controller " . get_class($this));
        }
    }

    /**
     * Before filter - called before an action method.
     *
     * @return void
     */
    protected function before() : void
    {

    }

    /**
     * After filter - called after an action method.
     *
     * @return void
     */
    protected function after() : void
    {

    }

    /**
     * Redirect to currently url
     * @param $url
     */
    protected function redirect($url) : void {
        header("Location: $url");
        exit;
    }

    /**
     * Send response
     * @param int $status
     * @param array $msg
     */
    protected function send(int $status = 200, array $msg) {
        $this->response->sendStatus($status);
        $this->response->setContent($msg);
        $this->response->render();
    }

    /**
     * This method create object of model
     * @param string $model
     * @return object
     */
    protected function getModel(string $model) {
        $file = $this->config->getConfig('root_path') .'/App/Models/'. ucfirst($model) . '.php';

        // check exists file
        if (file_exists($file)) {
            require_once $file;

            $model = "\\App\\Models\\" . str_replace('/', '', ucwords($model, '/'));
            // check class exists
            if (class_exists($model))
                return new $model;
            else
                throw new \Exception(sprintf('{ %s } this model class not found', $model));
        } else {
            throw new \Exception(sprintf('{ %s } this model file not found', $file));
        }
    }
}
