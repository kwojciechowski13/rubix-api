<?php

namespace Core\Core;

use PDO;
use App\Config\Config;

/**
 * Base model
 *
 */
abstract class Model
{
    /**
     * Get the PDO database connection
     *
     * @return mixed
     */
    protected static function getDB() : object {
        return PDO;
    }

    /**
     * Insert data to database
     *
     * @param $query
     * @return bool
     */
    protected function insert($query) : bool {
        return true;
    }

    /**
     * Select data from database
     *
     * @param $query
     * @return array
     */
    protected function select($query) : array {
        return [];
    }

    /**
     * Update data in database
     *
     * @param $query
     * @return bool
     */
    protected function update($query) : bool {
        return true;
    }

    /**
     * Delete data from database
     *
     * @param array $query
     * @return bool
     */
    protected function delete(array $query) : bool {
        return true;
    }
}
