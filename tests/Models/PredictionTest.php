<?php

use PHPUnit\Framework\TestCase;

class PredictionTest extends TestCase
{
    protected $prediction;


    public function setUp(): void
    {
        $this->prediction = new \App\Models\Prediction;
    }

    /** @test */
    public function eventIdCanBySet() : void {
        $this->prediction->setEventId(123);

        $this->assertEquals($this->prediction->getEventId(), 123);
    }

    /** @test */
    public function eventIdIsNumber() : void {
        $this->prediction->setEventId(123);

        $this->assertIsInt($this->prediction->getEventId());
    }

    /** @test */
    public function getPredictionById() : void {

        $this->prediction->setEventId(123);
        $this->prediction->setMarketType(1);
        $this->prediction->setPrediction('string');
        $this->prediction->setStatus(2);
        $this->prediction->setCreatedAt('1630309390');
        $this->prediction->setUpdatedAt('1630309390');

        $predictionData = $this->prediction->getPredictionById(54);

        $this->assertArrayHasKey('event_id', $predictionData);
        $this->assertArrayHasKey('market_type', $predictionData);
        $this->assertArrayHasKey('prediction', $predictionData);
        $this->assertArrayHasKey('status', $predictionData);
        $this->assertArrayHasKey('created_at', $predictionData);
        $this->assertArrayHasKey('updated_at', $predictionData);

        $this->assertEquals($predictionData['event_id'], 123);
        $this->assertEquals($predictionData['market_type'], 1);
        $this->assertEquals($predictionData['prediction'], 'string');
        $this->assertEquals($predictionData['status'], 2);
        $this->assertEquals($predictionData['created_at'], '1630309390');
        $this->assertEquals($predictionData['updated_at'], '1630309390');
    }

}