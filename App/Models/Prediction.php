<?php

namespace App\Models;

use Core\Core\Model;

class Prediction extends Model
{

    /**
     * @var int
     */
    protected int $id;

    /**
     * @var int
     */
    protected int $event_id;

    /**
     * @var int 0 '1x2' || 1 'correct_score'
     */
    protected int $market_type;

    /**
     * @var string
     */
    protected string $prediction;

    /**
     * @var int 0 'win' || 1 'lost' || 2 'unresolved'
     */
    protected int $status;

    /**
     * @var string
     */
    protected string $created_at;

    /**
     * @var string
     */
    protected string $updated_at;

    /**
     * This method create new prediction
     *
     * @param array $data
     * @return bool
     */
    public function createPrediction() : bool{

        return true;
//        return $this->insert();
    }

    /**
     * This method return all predictions
     *
     * @param array $data
     * @return array
     */
    public function getAllPrediction(array $data) : array {

//        return $this->select($data);
    }

    /**
     * This method return database prediction object
     * @param int $id
     * @return array
     */
    public function getPredictionById(int $id) : array {

        $data = [
            'id' => $id,
            'event_id' => $this->getEventId(),
            'market_type' => $this->getMarketType(),
            'prediction' => $this->getPrediction(),
            'status' => $this->getStatus(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpadatedAt()
        ];

        return $data;
//        return $this->select($data);
    }



    /**
     * Set prediction id
     *
     * @param int $id
     */
    public function setId(int $id){
        $this->id = $id;
    }

    /**
     * Get prediction id
     *
     * @return int
     */
    public function getId() : int {
        return $this->id;
    }

    /**
     * @param int $eventId
     */
    public function setEventId(int $eventId) : void{
        $this->event_id = $eventId;
    }

    /**
     * @return int
     */
    public function getEventId() : int {
        return $this->event_id;
    }

    /**
     * @param int $marketType 0 '1x2' || 1 'correct_score'
     */
    public function setMarketType(int $marketType) : void {
        $this->market_type = $marketType;
    }

    /**
     * @return int
     */
    public function getMarketType() : int {
        return $this->market_type;
    }

    /**
     * @param string $prediction
     */
    public function setPrediction(string $prediction) : void {
        $this->prediction = $prediction;
    }

    /**
     * @return string
     */
    public function getPrediction() : string {
        return $this->prediction;
    }

    /**
     * @param int $status 0 'win' || 1 'lost' || 2 'unresolved'
     */
    public function setStatus(int $status) : void {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getStatus() : int {
        return $this->status;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt(string $createdAt) : void {
        $this->created_at = $createdAt;
    }

    /**
     * @return string
     */
    public function getCreatedAt() : string {
        return $this->created_at;
    }

    /**
     * @param string $updatedAt
     */
    public function setUpdatedAt(string $updatedAt) : void {
        $this->updated_at = $updatedAt;
    }

    /**
     * @return string
     */
    public function getUpadatedAt() : string {
        return $this->updated_at;
    }


}