<?php

namespace App\Models;

use PDO;
use \Core\Model;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class User extends Model
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public static function getAll()
    {
        $db = static::getDB();
        $stmt = $db->query('SELECT * FROM users');
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    }
    public function userAdd($value)
    {
        $data = [
            'tableName' => 'users',
            'fields' => $value
        ];

        $this->insert($data);

    }

    public function deleteUser($id){
        return $this->delete('users', $id);
    }

    public function getUser($userName){
        $user=$this->select($userName);
        return $user;
        $user = [
                'avatar' => '../assets/images/!logged-user.jpg',
                'name' => 'user',
                'projects' => 
                [
                    [
                        'discription' => 'Lorem ipsom dolor sit.',
                        'title' => 'Porto Template',
                        'color' => 'red'
                    ],
                    [
                        'discription' => 'Lorem ipsom dolor sit amet',
                        'title' =>  'Tucson HTML5 Template',
                        'color' => 'green'
                    ],
                    [
                       'discription' => 'Porto HTML5 Template',
                       'title' =>  'Porto Template',
                       'color' => 'blue'
                    ],
                    [
                        'discription' => 'Lorem ipsom dolor sit.',
                        'title' =>  'Tucson Template',
                        'color' => 'orange'
                    ]
                ],
                'messages' =>
                [
                    [
                        'avatar' => '../assets/images/!sample-user.jpg',
                        'message' => 'wiadomosc'
                    ],
                    [
                       'avatar' => '../assets/images/!sample-user.jpg',
                        'message' => 'wiadomosc'
                    ],

                    [
                       'avatar' => '../assets/images/!sample-user.jpg',
                        'message' => 'wiadomosc'
                    ],
                    [
                        'avatar' => '../assets/images/!sample-user.jpg',
                        'message' => 'wiadomosc'
                    ]


                ]
                    

    ];




       

        return $user;
    }
}
