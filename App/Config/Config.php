<?php

namespace App\Config;

use Monolog\Logger;

/**
 * Application configuration
 *
 */
class Config
{

    /**
     * Database host
     * @var string
     */
    const DB_HOST = 'localhost';

    /**
     * Database name
     * @var string
     */
    const DB_NAME = 'praktyki';

    /**
     * Database user
     * @var string
     */
    const DB_USER = 'root';

    /**
     * Database password
     * @var string
     */
    const DB_PASSWORD = 'root';

    /**
     * Show or hide error messages on screen
     * @var boolean
     */
    const SHOW_ERRORS = false;

    private static $instance;

    private array $config;

    public function __construct() {

        $this->config = [
            'app_path' => $_SERVER['DOCUMENT_ROOT'],
            'root_path' => $_SERVER['PWD']
        ];
    }

    /**
     * Get application config
     * @param string|null $key
     * @return
     */
    public function getConfig(string $key = null)  {
        if(is_null($key))
            return $this->config;
        else
            return $this->config[$key];
    }

    public static function getInstance() {
        if(self::$instance === null) {
            self::$instance = new Config();
        }
        return self::$instance;
    }
}
