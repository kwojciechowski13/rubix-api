<?php

// 'path/to/action' => ['controller' => 'NameController, 'action' => 'nameAction']

return [
    '' => ['controller' => 'Home', 'action' => 'index'],


    'prediction/add' => ['controller' => 'PredictionController', 'action' => 'add'],

    'prediction/get' => ['controller' => 'PredictionController', 'action' => 'get'],

    'prediction/update' => ['controller' => 'PredictionController', 'action' => 'update']


];


