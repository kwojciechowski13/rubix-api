<?php

namespace App\Controllers;

use Core\Core\Controller;
use \App\Models\Prediction;

class PredictionController extends Controller
{

    /**
     * This action create new prediction
     * @throws \Exception
     */
    public function addAction() : void {
        if($this->request->getType() != 'POST')
            throw new \Exception('No route matched.', 404);

        $prediction = $this->getModel('prediction');
        $prediction->setEventId($this->request->post('event_id'));
        $prediction->setPrediction($this->request->post('prediction'));
        $prediction->setMarketType($this->request->post('market_type'));
        $prediction->setStatus(random_int(0,2));
        $prediction->setCreatedAt(time());
        $prediction->setUpdatedAt(time());

        if($prediction->createPrediction())
            $this->send(200, ['data' => 'Prediction created']);
        else
            throw new \Exception("No can't created prediction.", 404);
    }

    /**
     * This action gets all predictions
     */
    public function getAction() : void {
        $prediction = $this->getModel('prediction');
        $predictionList = $prediction->getAllPrediction();
        $this->send(200, ['data' => $predictionList]);
    }

    /**
     * This action update isset prediction
     * @throws \Exception
     */
    public function updateAction() : void {
        if($this->request->getType() != 'PUT')
            throw new \Exception('No route matched.', 404);

        $predictionModel = $this->getModel('prediction');
        $prediction = $predictionModel->getPredictionById($this->request->get('id'));
        $prediction->setStatus($this->request->get('status'));

        if(!$prediction->update())
            throw new \Exception("No can't updated prediction.", 404);

        $this->send(200, ['data' => json_encode($prediction)]);
    }
}