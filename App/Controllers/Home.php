<?php

namespace App\Controllers;

use Core\Core\Controller;

/**
 * Home controller
 *
 * PHP version 7.0
 */
class Home extends Controller
{

    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction() : void
    {

        echo 'Welcome to Rubix Api';
    }
}
