<?php

/**
 * Front controller
 *
 * PHP version 7.0
 */

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';


/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('Core\Core\Error::errorHandler');
set_exception_handler('Core\Core\Error::exceptionHandler');

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\ErrorHandler;

$logger = new Logger('AppLogger');
$logger->pushHandler(new StreamHandler(dirname(__DIR__) . '/logs/app.log', Logger::DEBUG));
ErrorHandler::register($logger);

App\Config\Config::getInstance();

/**
 * Routing
 */
$router = new Core\Core\Router();

// Add the routes
$routers = include '../App/Config/router.php';
foreach ($routers as $url => $params) {
    $router->add($url, $params);
}

//$router->add('{controller}/{action}');

$router->dispatch($_SERVER['REQUEST_URI']);
